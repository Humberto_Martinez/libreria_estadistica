﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace L_Estadistica
{
    public class Estadistica
    {
        private int[] Data { get; set; }

        public Estadistica(int[] data)
        {
            Data = data;
        }

        public int Moda()
        {
            int MODA = Data[0];
            int NUMERO = 0;

            for (int i = 0; i < Data.Length; i++)
            {
                int REPETIR = 0;
                for(int j = 0; j < Data.Length; j++)
                {
                    if (Data[j] == Data[i]) REPETIR++; 
                }
                if (REPETIR > NUMERO)
                {
                    MODA = Data[i];
                    NUMERO = REPETIR;
                }
            }
            return MODA;
        }


        public double Mediana()
        {
            Array.Sort(Data);

            double MEDIANA;
            int MITAD = Data.Length / 2;

            if(Data.Length % 2 == 0)
            {
                MEDIANA = (((MITAD + 1) + MITAD) / 2.0);
            }
            else
            {
                MEDIANA = (Data[MITAD]);
            }
            return MEDIANA;
        }
        
        
        public double Promedio()
        {
            int TOTAL = 0;
            for (int i = 0; i < Data.Length; i++)
            {
                TOTAL = TOTAL + Data[i];
            }
            double TOTAL_ARRAY = Data.Length, PROMEDIO = TOTAL / TOTAL_ARRAY;
            return PROMEDIO;
        }

        public double Desviacion()
        {
            double MEDIA = Promedio();
            int tamano = Data.Length;
            double SUMATORIA = 0;
            double DESV = 0;

            for(int i = 0;i < Data.Length;i++)
            {
                SUMATORIA = SUMATORIA + Math.Pow(Data[i] - MEDIA, 2);
            }
            DESV = Math.Sqrt(SUMATORIA / (tamano - 1));
            return DESV;
        }

        public double Varianza()
        {
            double MEDIA = Promedio();
            int tamano = Data.Length;
            double ACUMULADO = 0;
            double VARN = 0;

            for(int i = 0; i < Data.Length; i++)
            {
                ACUMULADO = ACUMULADO + Math.Pow(Data[i] - MEDIA, 2);
            }
            VARN = ACUMULADO / (tamano - 1);
            return VARN;
        }


        public double Cuartiles()
        {
            double Q1 = 0;
            double Q2 = 0;
            double Q3 = 0;

            Array.Sort(Data);

                Q1 = Math.Round(Data.Length * 0.25);

                Q2 = Math.Round(Data.Length * 0.50);

                Q3 = Math.Round(Data.Length * 0.75);


                /*if (Q1 == Data[i])
                {
                    Console.WriteLine("El CUARTIL 1 se encuentra en la posición " + Q1 + " y es el dato " + Data[i]);
                }
                if (Q2 == Data[i])
                {
                    Console.WriteLine("El CUARTIL 2 se encuentra en la posición " + Q2 + " y es el dato " + Data[i]);
                }
                if (Q3 == Data[i])
                {
                    Console.WriteLine("El CUARTIL 3 se encuentra en la posición " + Q3 + " y es el dato " + Data[i]);
                }*/
            return Q1;
        }


        public double Coeficiente()
        {
            double COEFIVP = 0;
            double DESV = Desviacion();
            double MEDIA = Promedio();

            COEFIVP = DESV / MEDIA;
            return COEFIVP;
        }
    }
}
