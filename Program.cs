﻿using L_Estadistica;

int[] valores = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

//Media
Estadistica estadistica = new Estadistica(valores);
Console.WriteLine("La MEDIA es: " + estadistica.Promedio());

Console.WriteLine("\n");

//Mediana
Estadistica mediana = new Estadistica(valores);
Console.WriteLine("La MEDIANA es: " + mediana.Mediana());

Console.WriteLine("\n");

//Moda
Estadistica moda = new Estadistica(valores);
Console.WriteLine("La MODA es: " + moda.Moda());

Console.WriteLine("\n");

//Desviación estándar
Estadistica desviacion = new Estadistica(valores);
Console.WriteLine("La DESVIACIÓN es: " + desviacion.Desviacion());

Console.WriteLine("\n");

//Varianza
Estadistica varianza = new Estadistica(valores);
Console.WriteLine("La VARIANZA es: " + varianza.Varianza());

Console.WriteLine("\n");

//Coeficiente de variación de Pearson
Estadistica coeficiente = new Estadistica(valores);
Console.WriteLine("El COEFICIENTE DE VARIACIÓN DE PEARSON es: " + coeficiente.Coeficiente());